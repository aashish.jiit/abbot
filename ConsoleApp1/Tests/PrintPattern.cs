﻿using System;
using System.Collections.Generic;
using System.Net.Http.Headers;
using System.Text;

namespace AutoBOT
{
    class Printpattern
    {
        public void pattern()
        {
            int a = 0;
            int b = 0;
        repeat:
            try
            {
                Console.Clear();
                Console.WriteLine("\n\n\t\t\t**** Hey " + Userlogin.usernamepassed + "! So another challenge for AUTOBOT \"AUTO-CHATBOT\" **** \n\t\t\t   Drawing a pattern, My Favourite!! ");


                Console.WriteLine("Rectangle or triangle?");
                string patternsel = Console.ReadLine();

                //Code to print a rectangle OR Triangle with user entered rows and columns
                switch (patternsel.ToUpper().Trim())
                {
                    case "RECTANGLE":

                        Console.WriteLine("Enter the number of rows to print in pattern");
                        a = Convert.ToInt32(Console.ReadLine());

                        Console.WriteLine("Enter the number of column to print in pattern");
                        b = Convert.ToInt32(Console.ReadLine());

                        int[,] entry = new int[a, b];

                        for (int i = 0; i <= a; i++)
                        {
                            for (int j = 0; j <= b; j++)
                            {
                                Console.Write("*");
                            }
                            Console.WriteLine("\n");
                        }
                        break;

                    case "TRIANGLE":
                        Console.WriteLine("Enter the number of rows to print in the Triangle");
                        int ro = Convert.ToInt32(Console.ReadLine());
                        for (int r = 0; r < ro; r++)
                        {
                            for (int c = 0; c < r + 1; c++)
                            {
                                Console.Write("*");
                            }

                            Console.Write("\n");
                        }
                       
                        break;

                    default:
                        Console.WriteLine("\n\n\t\t\t\tInvalid Entry, please try again!" + Userlogin.usernamepassed);
                        break;
                        
                        
                }

            }
            catch (Exception)
            {
                Console.WriteLine("\t\t \t  Ah! snap.." + Userlogin.usernamepassed + " that was an Invalid data format. Click ENTER and Try again.. ");
                Console.ReadKey();
                goto repeat;
            }





            Console.Write("\n\t\t \tDo you want me to do it for you again {0} ?(Press Y/y to cont. or else to Exit): ", Userlogin.usernamepassed);
            string recheck = Console.ReadLine();
            if (recheck == "Y" || recheck == "y")
            {
                goto repeat;
            }
            else
            {
                Console.Write("\n\t\t Do you want me to take you to the main menu page {0} ?(Press Y/y to cont. or else to Exit): ", Userlogin.usernamepassed);
                string recheck2 = Console.ReadLine();
                if (recheck2 == "Y" || recheck2 == "y")
                {
                    Menuselection.menuSel();
                }
                else
                {

                    Console.WriteLine("\n\n\t\t\t\t******Thanks for using our application******");
                    Console.WriteLine("\n\n\t\t\t\t\t ******EXITING******");

                    Console.ReadKey();
                }

            }

        }
    }
}

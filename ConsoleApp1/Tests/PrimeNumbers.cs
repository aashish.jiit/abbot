﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AutoBOT
{
    class Primenumbers
    {
        public void primePrint()
        {

            int prime = 0;

           
            repeat:
            Console.Clear();
            Console.WriteLine("\n\n\t\t\t**** Hey " + Userlogin.usernamepassed + "! So another challenge for AUTOBOT \"AUTO-CHATBOT\" **** \n\t\t\t\tGive me a number to get all prime numbers ");

            try
            {
                Console.Write("\n\n\t\t\t\t\t  Enter the number: ");
                
                prime = Convert.ToInt32(Console.ReadLine());
                if (prime < 2)
                {
                    Console.WriteLine("\n\n\t\t\t\t  No prime number found 0 to " + prime + " !!");
                }

                //loop for 2 to entered number
                for (int i = 2; i <= prime; i++)
                {
                    bool checkprime = true;

                    // not using the check where loop has iterations till number/2 as here I need to print all primes till that number
                    for (int j = 2; j <= prime ; j++)  
                        {
                        //if mod with a number result in 0 and number not equal to denominator then not prime
                            if (i != j && i % j == 0)
                            {
                                //Console.WriteLine("Number: {0} is  prime", i);
                                checkprime = false;
                                break;
                            }
                            
                        }
                    // if checkprime flag still true print the number as prime
                        if (checkprime)
                        {
                        if (i == prime)
                        {
                            Console.Write(i + " ");
                        }else
                            Console.Write( i + ", ");
                        }
                    
                }
            }
            catch (Exception)
            {
                Console.Write("\n\n\t\t \t  Ah! snap.." + Userlogin.usernamepassed + " that was an Invalid data format. Click ENTER and Try again.. ");
                Console.ReadKey();
                goto repeat;
            }


            Console.WriteLine("\n\n\t\t   \t\t\t  That's it buddy!");
            Console.WriteLine("\n\n\t\t \tSo " + Userlogin.usernamepassed + "! Result displayed above for reference");



            Console.Write("\n\t\t \tDo you want me to do it for you again {0} ?(Press Y/y to cont. or else to Exit): ", Userlogin.usernamepassed);
            string recheck = Console.ReadLine();
            if (recheck == "Y" || recheck == "y")
            {
                goto repeat;
            }
            else
            {
                Console.Write("\n\t\t Do you want me to take you to the main menu page {0} ?(Press Y/y to cont. or else to Exit): ", Userlogin.usernamepassed);
                string recheck2 = Console.ReadLine();
                if (recheck2 == "Y" || recheck2 == "y")
                {
                    Menuselection.menuSel();
                }
                else
                {

                    Console.WriteLine("\n\n\t\t\t\t******Thanks for using our application******");
                    Console.WriteLine("\n\n\t\t\t\t\t ******EXITING******");

                    Console.ReadKey();
                }

            }

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AutoBOT
{
    class Menubynumber 
    {

        public void ExecuteNumberMenu(string inputnumbermenu)
        {
            
            
            Console.Clear();
            switch (inputnumbermenu)
            {
                case "1":
                    
                    Console.WriteLine("\n\n\n\t\t \t \t Hey! " + Userlogin.usernamepassed + " So you want- A method to add 5 numbers.");
                    Addfivenumbers fn = new Addfivenumbers();
                    fn.addFiveNumber();

                    break;

                case "2":
                    Console.WriteLine("\n\n\n\t\t \t \t Hey! " + Userlogin.usernamepassed + "So you want- A method to add user entered numbers.");
                    Askaddfivenumbers aadd = new Askaddfivenumbers();
                    aadd.askAddFiveNumber();
                    break;

                case "3":
                   
                    Console.WriteLine("\n\n\n\t\t \t \t Hey! " + Userlogin.usernamepassed + "So you want- Check if the number entered is a Prime number.");
                    CheckPrimenumber cpr = new CheckPrimenumber();
                    cpr.checkPr();
                    break;

                case "4":
                    
                    Console.WriteLine("\n\n\n\t\t \t \t Hey! " + Userlogin.usernamepassed + "So you want- Generate prime numbers upto entered number");
                    Primenumbers pr = new Primenumbers();
                    pr.primePrint();
                    break;

                case "5":
                    Console.WriteLine("So you want- Number Sorting.");
                    SortNumUptoN sn = new SortNumUptoN();
                    sn.numberSort();
                    break;

                case "6":
                    Console.WriteLine("\n\n\n\t\t \t \tHey! " + Userlogin.usernamepassed + "So you want- Generate fibonacci series");
                    fibSeriesUptoN fib = new fibSeriesUptoN();
                    fib.fibonacciSeries();
                    break;

                case "7":
                    Console.WriteLine("So you want- Print a string in reverse.");
                    ReverseString rst = new ReverseString();
                    rst.reverseStr();
                    break;

                case "8":
                    
                    Console.WriteLine("\n\n\n\t\t \t \t Hey! " + Userlogin.usernamepassed + "So you want- Print a number in reverse order!");
                    ReverseNum rnm = new ReverseNum();
                    rnm.reverse_num();
                    break;

                case "9":
                   
                    Console.WriteLine("\n\n\n\t\t \t \t Hey! " + Userlogin.usernamepassed + "So you want- A program to print a pattern(*).");
                    Printpattern pp = new Printpattern();
                    pp.pattern();
                    break;

                default:
                    Console.WriteLine("\n\t\t \t\t\t " + Userlogin.usernamepassed + " It was an Invalid Entry");

                    
                        Console.Write("\n\t\t Do you want me to take you to the main menu page {0} ?(Press Y/y to cont. or else to Exit): ", Userlogin.usernamepassed);
                        string recheck2 = Console.ReadLine();
                        if (recheck2 == "Y" || recheck2 == "y")
                        {
                            Menuselection.menuSel();
                        }
                        else
                        {

                            Console.WriteLine("\n\n\t\t\t\t******Thanks for using our application******");
                            Console.WriteLine("\n\n\t\t\t\t\t ******EXITING******");

                            Console.ReadKey();
                        }

                    


                    break;
            }

        }
    }
}

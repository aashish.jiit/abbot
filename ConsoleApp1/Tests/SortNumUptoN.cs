﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AutoBOT
{
    class SortNumUptoN
    {
        int n, temp;
        public void numberSort()
        {
        repeat:
            try
            {
                Console.Clear();
                Console.WriteLine("\n\n\t\t\t**** Hey " + Userlogin.usernamepassed + "! So another challenge for AUTOBOT \"AUTO-CHATBOT\" , My Favourite!!");


                Console.Write("\n\n\t\t\t\tEnter number N up to which numbers are to be sorted: ");
                n = int.Parse(Console.ReadLine());



                int[] numArr = new int[n];//define empty array



                //input numbers from user up to N and get it stored in an integer array
                for (int i = 0; i < n; i++)
                {
                    Console.Write("\n\n\t\tEnter number {0}: ", i + 1);
                    numArr[i] = Convert.ToInt32(Console.ReadLine());
                }



                //display unsorted array
                Console.Write("\n\n\t\t\tUnsorted array is: ");
                for (int i = 0; i < n; i++)
                {
                    Console.Write(numArr[i] + "\t");
                }



                //Sorting logic
                for (int j = 0; j <= numArr.Length - 2; j++)
                {
                    for (int i = 0; i <= numArr.Length - 2; i++)
                    {
                        if (numArr[i] > numArr[i + 1])
                        {
                            temp = numArr[i + 1];
                            numArr[i + 1] = numArr[i];
                            numArr[i] = temp;
                        }
                    }
                }



                //display sorted array
                Console.Write("\n\n\n\t\t\tSorted array is: ");
                for (int i = 0; i < n; i++)
                {
                    Console.Write(numArr[i] + "\t");
                }
                Console.Write("\t\t \tPress ENTER ");
                Console.ReadKey();
            }
            catch (Exception)
            {
                Console.WriteLine("\t\t \t  Ah! snap.." + Userlogin.usernamepassed + " that was an Invalid data format. Click ENTER and Try again.. ");
                Console.ReadKey();
                goto repeat;
            }

            Console.Write("\n\t\t \tDo you want me to do it for you again {0} ?(Press Y/y to cont. or else to Exit): ", Userlogin.usernamepassed);
                string recheck = Console.ReadLine();
                if (recheck == "Y" || recheck == "y")
                {
                    goto repeat;
                }
                else
                {
                    Console.Write("\n\t\t Do you want me to take you to the main menu page {0} ?(Press Y/y to cont. or else to Exit): ", Userlogin.usernamepassed);
                    string recheck2 = Console.ReadLine();
                    if (recheck2 == "Y" || recheck2 == "y")
                    {
                        Menuselection.menuSel();
                    }
                    else
                    {

                        Console.WriteLine("\n\n\t\t\t\t******Thanks for using our application******");
                        Console.WriteLine("\n\n\t\t\t\t\t ******EXITING******");

                        Console.ReadKey();
                    }

                }
            
    }  }
}

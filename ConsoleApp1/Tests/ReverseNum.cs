﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace AutoBOT
{
    class ReverseNum
    {
        public void reverse_num()
        {
        start:

            long original_number;
            string result = "";
            
            Console.Clear();
            Console.WriteLine("\n\n\t\t\t**** Hey " + Userlogin.usernamepassed + "! So another challenge for AUTOBOT \"AUTO-CHATBOT\" **** \n\t\t\t   Give me a number you want to print in reverse ");
            Console.Write("\n\n\t\t\t\t Enter a number you want to reverse: ");
            

            string str = Console.ReadLine();
            if (string.IsNullOrWhiteSpace(str))
            {
                Console.WriteLine("\n\n\t\t\tInvalid Entry, Just white space or Blank string not allowed!!  ");
                goto end;
            }
            Regex reg_alpha = new Regex("[a-zA-Z]");
            Regex reg_specialchar = new Regex("[^a-zA-Z0-9-]");
            Regex minus = new Regex("[0-9]{1,}[-][0-9]*");
            Regex sub = new Regex("[-]{2,}");

            if (reg_alpha.IsMatch(str) || reg_specialchar.IsMatch(str))
            {
                Console.WriteLine("\n\n\t\t\tYou must have entered aplhabets or special characters.Press ENTER");
                Console.ReadKey();
                goto end;
            }
            else if (minus.IsMatch(str) || sub.IsMatch(str))
            {
                Console.WriteLine("Invalid number with (-) sign in between. Please enter a valid number.");
                Console.ReadKey();
                goto end;
            }
           
                
           

            else
            {
                original_number = Convert.ToInt64(str);
                int count = 0;
                foreach (char c in str)
                {
                    count++;
                }

                char[] arr = new char[count];

                if (original_number > 0)
                {

                    for (int i = 0; count > 0; i++)
                    {
                        arr[i] = str[count - 1];
                        count = count - 1;
                    }
                    foreach (char c in arr)
                    {
                        result = result + c;
                    }

                    if (str == result)
                    {
                        Console.WriteLine("\n\n\t\t\tThis number is palindrome. Reverse of " + str + " is same as {0}.", result);

                        goto end;
                    }
                    else
                    {
                        Console.WriteLine("\n\n\t\t\t\t\tReverse of {0} is {1}", str, result);
                        goto end;
                    }
                }


                else if (original_number < 0)
                {
                    Console.WriteLine("\n\n\t\t\t\t Please enter a valid number, It can't be negative. Press ENTER");
                    Console.ReadKey();
                    goto start;
                }

                else if (original_number == 0)
                {
                    Console.WriteLine("\n\n\t\t\t\tPlease enter a valid number, It can't be zero, Press ENTER");
                    Console.ReadKey();
                    goto start;
                }

            }

            

        end:
            Console.Write("\n\t\t \tDo you want me to do it for you again {0} ?(Press Y/y to cont. or else to Exit): ", Userlogin.usernamepassed);
            string recheck = Console.ReadLine();
            if (recheck == "Y" || recheck == "y")
            {
                goto start;
            }
            else
            {
                Console.Write("\n\t\t Do you want me to take you to the main menu page {0} ?(Press Y/y to cont. or else to Exit): ", Userlogin.usernamepassed);
                string recheck2 = Console.ReadLine();
                if (recheck2 == "Y" || recheck2 == "y")
                {
                    Menuselection.menuSel();
                }
                else
                {

                    Console.WriteLine("\n\n\t\t\t\t******Thanks for using our application******");
                    Console.WriteLine("\n\n\t\t\t\t\t ******EXITING******");

                    Console.ReadKey();
                }

            }
            
        }

    }
}

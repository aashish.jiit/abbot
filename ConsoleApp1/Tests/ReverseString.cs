﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AutoBOT
{
    class ReverseString
    {
        int n;
        public void reverseStr()
        {
        //KeyPressEventArgs e;
        // string[] str = new string[] { };//declare empty string
        repeat:
            try
            {
                Console.Clear();
                Console.WriteLine("\n\n\t\t\t**** Hey " + Userlogin.usernamepassed + "! So another challenge for AUTOBOT \"AUTO-CHATBOT\" , My Favourite!! ");

                Console.Write("\n\n\t\t\t\tEnter the string to be reversed: ");
                string str = Console.ReadLine();
                char[] charstr = str.ToCharArray();//converting the entered string into character array
                if (charstr[0] == '\t' || (int)charstr[0] == 32)
                {
                    Console.WriteLine("\n\n\t\t\t\tEnter or space in the starting is not allowed....");
                   
                }

                string revStr = "";//empty string to store reverse of str

                //reverse the character string
                for (int i = charstr.Length - 1; i >= 0; i--)
                {
                    revStr = revStr + charstr[i];
                }
                Console.Write("\n\n\t\t\t\t\t\t Result: ");
                //display the reversed string
                for (int i = 0; i < revStr.Length; i++)
                {
                    Console.Write(revStr[i]);
                }

                if (str == revStr)
                {
                    Console.WriteLine("\n\n\t\t\t\t\tThis string is a PALINDROME");
                    
                }

            }
            catch (Exception)
            {
                Console.WriteLine("\t\t \t  Ah! snap.." + Userlogin.usernamepassed + " that was an Invalid data format. Click ENTER and Try again.. ");
                Console.ReadKey();
                goto repeat;
            }

            Console.Write("\n\t\t \tDo you want me to do it for you again {0} ?(Press Y/y to cont. or else to Exit): ", Userlogin.usernamepassed);
            string recheck = Console.ReadLine();
            if (recheck == "Y" || recheck == "y")
            {
                goto repeat;
            }
            else
            {
                Console.Write("\n\t\t Do you want me to take you to the main menu page {0} ?(Press Y/y to cont. or else to Exit): ", Userlogin.usernamepassed);
                string recheck2 = Console.ReadLine();
                if (recheck2 == "Y" || recheck2 == "y")
                {
                    Menuselection.menuSel();
                }
                else
                {

                    Console.WriteLine("\n\n\t\t\t\t******Thanks for using our application******");
                    Console.WriteLine("\n\n\t\t\t\t\t ******EXITING******");

                    Console.ReadKey();
                }

            }
        }

    }
}

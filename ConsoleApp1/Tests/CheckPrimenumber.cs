﻿using System;
using System.Collections.Generic;
using System.Reflection.Metadata.Ecma335;
using System.Text;

namespace AutoBOT
{
    class CheckPrimenumber
    {
        public void checkPr()
        {
         
        // flow jumps here if user wants more attempts    
        repeat:
            Console.Clear();
            Console.WriteLine("\n\n\t\t\t**** Hey " + Userlogin.usernamepassed + "! So another challenge for AUTOBOT \"AUTO-CHATBOT\" **** \n\t\t\t   Give me a number to to check if it is a Prime number ");

            int primechk = 0;

            try
            {
                Console.Write("\n\n\t\t\t\t\t  Enter the number: ");
                primechk = Convert.ToInt32(Console.ReadLine());
                if (primechk < 2)
                {
                    Console.WriteLine("\n\n\t\t\t\t\tNaahh!.. " +  primechk + " is not prime!!");
                    goto next;
                }
                    //flag to track for prime number            
                    bool checkprime = true;

                // loop strat from 2 as every no. mod by 1 is 0 , so can ignore it
                // to save extra checks , as mod of number divided by more than number/2 cannot be 0

                for (int j = 2; j <= primechk/2 ; j++)  
                    {
                        if (primechk % j  == 0)
                        {
                        
                        checkprime = false;

                        // to move out of for loop and and print result
                        break;  
                        }

                    }
                    if (checkprime)
                    {
                        Console.Write("\n\n\t\t\t\t\tWuhu!.. " + primechk + " is prime.. ");
                    }
                else
                {
                        //when checkprime set to false           
                        Console.Write("\n\n\t\t\t\t\tNaahh!.. " + primechk + " is not prime.. ");
                    
                }

                

            }
            catch (Exception)
            {
                Console.WriteLine("\t\t \t  Ah! snap.." + Userlogin.usernamepassed + " that was an Invalid data format. Click ENTER and Try again.. ");
                Console.ReadKey();
                goto repeat;
            }


            Console.WriteLine("\n\n\t\t\t \t\t  That's it buddy!");
            Console.WriteLine("\n\n\t\t\t So " + Userlogin.usernamepassed + "! Result for the number: " + primechk + " displayed above for reference");

            next:  // flow jumps here if less than 2 value is entered

            
            Console.Write("\n\t\t \tDo you want me to do it for you again {0} ?(Press Y/y to cont. or else to Exit): ", Userlogin.usernamepassed);
            string recheck = Console.ReadLine();
            if (recheck == "Y" || recheck == "y")
            {
                goto repeat;
            }
            else
            {
                Console.Write("\n\t\t Do you want me to take you to the main menu page {0} ?(Press Y/y to cont. or else to Exit): ", Userlogin.usernamepassed);
                string recheck2 = Console.ReadLine();
                if (recheck2 == "Y" || recheck2 == "y")
                {
                    Menuselection.menuSel();
                }
                else
                {

                    Console.WriteLine("\n\n\t\t\t\t******Thanks for using our application******");
                    Console.WriteLine("\n\n\t\t\t\t\t ******EXITING******");

                    Console.ReadKey();
                }

            }
        }
    }
}

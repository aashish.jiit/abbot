﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AutoBOT
{
    class fibSeriesUptoN
    {
      
        public void fibonacciSeries()
        {

        start:
            Console.Clear();
            Console.WriteLine("\n\n\t\t\t**** Hey " + Userlogin.usernamepassed + "! So another challenge for AUTOBOT \"AUTO-CHATBOT\" -  Fibonacci Series ");

            int n;


            int flag = 1;
        
            Console.Write("\n\n\t\t \tEnter number N up to which Fibonacci Series need to be displayed: ");
            string input = Console.ReadLine();
            string input3 = input;
            //Console.WriteLine((int)input);
            char[] input1 = input.ToCharArray();//convert entered string to characters

            //LOGIC to check if the input string contains only number and no other charcter, special char, space etc
            for (int i = 0; i < input.Length; i++)
            {
                int ASCII = (int)input1[i];
                //Console.WriteLine(ASCII);
                if (ASCII >= 48 && ASCII <= 57)
                    flag = 0;
            }
            if (flag == 0) //if flag==0, only then we will print fib series
            {
                n = int.Parse(input3);

                int[] fbSeries = new int[n]; //define empty int array

                //1st and 2nd number of the Fibonacci series is always 0 and 1
                fbSeries[0] = 0;

                fbSeries[1] = 1;


                //checkPrimeNumber PrimeNumber = new checkPrimeNumber();


                Console.Write(fbSeries[0] + "\t");//display 1st number of Fibonacci series
                                                  //PrimeNumber.checkPrime(fbSeries[0]); //check if entered number is prime or not
                Console.Write(fbSeries[1] + "\t");//display 2nd number of Fibonacci series
                                                  //PrimeNumber.checkPrime(fbSeries[1]); //check if entered number is prime or not
                for (int i = 2; i < n; i++)
                {

                    fbSeries[i] = fbSeries[i - 1] + fbSeries[i - 2];
                    if (fbSeries[i] > n)
                        break;
                    Console.Write(fbSeries[i] + "\t");

                    //  PrimeNumber.checkPrime(fbSeries[i]); //check if entered number is prime or not
                }

            }
            else
            {
                Console.WriteLine("\n\n\t\t \tNo special character, space, alphanumeric value or enter is allowed.");
                goto end;
            }



        end:
            Console.Write("\n\n\t\t \tDo you want me to do it for you again {0} ?(Press Y/y to cont. or else to Exit): ", Userlogin.usernamepassed);
            string recheck = Console.ReadLine();
            if (recheck == "Y" || recheck == "y")
            {
                goto start;
            }
            else
            {
                Console.Write("\n\n\t\t Do you want me to take you to the main menu page {0} ?(Press Y/y to cont. or else to Exit): ", Userlogin.usernamepassed);
                string recheck2 = Console.ReadLine();
                if (recheck2 == "Y" || recheck2 == "y")
                {
                    Menuselection.menuSel();
                }
                else
                {

                    Console.WriteLine("\n\n\t\t\t\t******Thanks for using our application******");
                    Console.WriteLine("\n\n\t\t\t\t\t ******EXITING******");

                    Console.ReadKey();
                }


            }
        }
    }
}
